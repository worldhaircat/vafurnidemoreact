import logo from './logo.svg';
import './App.css';
import Navibar from './components/Navibar/Navibar';

import CartContext from './contexts/CartContext';

import furnitures from './fakeapi/furnitures';
import React, { createContext, useEffect, useState } from 'react';
import { ThemeProvider } from '@emotion/react';
import cusTheme from './helper/cusTheme';
import FootSec from './components/FootSec/FootSec';

function App() {
  let [isLoggedIn, setIsLoggedIn] = useState(sessionStorage.getItem('username') ? true : false);
  
  let logOut = e => {
    e.preventDefault();
    sessionStorage.setItem('username', '');
    setIsLoggedIn(false);
  };

  const [cart, setCart] = useState(
    sessionStorage.getItem('cart')
    ? JSON.parse(sessionStorage.getItem("cart"))
    :[]
  );

  return (
    <>
      <CartContext.Provider value={{ cart: cart, setCart: setCart }}>
        <ThemeProvider theme={cusTheme}>
          <Navibar loginLogout={
            {
              isLoggedIn: isLoggedIn,
              setIsLoggedIn: setIsLoggedIn,
              logOut: logOut
            }
          } />
          <FootSec />
        </ThemeProvider>
      </CartContext.Provider>
    </>
  );
}

export default App;
