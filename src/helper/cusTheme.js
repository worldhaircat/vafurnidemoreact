import { createTheme } from '@mui/material/styles';

const cusTheme = createTheme({
  palette: {
    primary: {
      main: '#47a0ff',
    },
    secondary: {
      main: '#334960',
    },
  },
});

export default cusTheme;