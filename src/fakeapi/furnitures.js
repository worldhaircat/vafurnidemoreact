let furnitures = [
    {
        id: 1,
        fname: 'Emerald Desk',
        mdate: new Date(2021,3,12),
        price: 1245000,
        imgUrl: 'emarald-desk.jpg',
        length: 2.34,
        width: 0.89,
        height: 0.45,
        cate: 'desk'
    },
    {
        id: 2,
        fname: 'Ruby Desk',
        mdate: new Date(2021,3,15),
        price: 1845000,
        imgUrl: 'ruby-desk.jpg',
        length: 2.37,
        width: 0.84,
        height: 0.95,
        cate: 'desk'
    },
    {
        id: 3,
        fname: 'Ocean Chair',
        mdate: new Date(2023,4,15),
        price: 1042000,
        imgUrl: 'ocean-chair.jpg',
        length: 0.37,
        width: 0.24,
        height: 0.45,
        cate: 'chair'
    },
    {
        id: 4,
        fname: 'Volcano Chair',
        mdate: new Date(2021,3,23),
        price: 652000,
        imgUrl: 'volcano-chair.jpg',
        length: 0.47,
        width: 0.34,
        height: 0.45,
        cate: 'chair'
    },
    {
        id: 5,
        fname: 'Forest Chair',
        mdate: new Date(2021,3,25),
        price: 734000,
        imgUrl: 'forest-chair.jpg',
        length: 0.37,
        width: 0.33,
        height: 0.95,
        cate: 'chair'
    },
    {
        id: 6,
        fname: 'Morning Desk',
        mdate: new Date(2020,9,8),
        price: 1129000,
        imgUrl: 'morning-desk.jpg',
        length: 2.31,
        width: 0.72,
        height: 0.88,
        cate: 'desk'
    },
    {
        id: 7,
        fname: 'Night Desk',
        mdate: new Date(2021,3,27),
        price: 923000,
        imgUrl: 'night-desk.jpg',
        length: 0.31,
        width: 0.42,
        height: 0.88,
        cate: 'desk'
    },
    {
        id: 8,
        fname: 'Alpha Bed',
        mdate: new Date(2023,4,13),
        price: 5234000,
        imgUrl: 'alpha-bed.jpg',
        length: 2.13,
        width: 1.89,
        height: 0.34,
        cate: 'bed'
    },
    {
        id: 9,
        fname: 'Beta Bed',
        mdate: new Date(2021,8,24),
        price: 6162000,
        imgUrl: 'beta-bed.jpg',
        length: 2.02,
        width: 1.73,
        height: 0.41,
        cate: 'bed'
    },
    {
        id: 10,
        fname: 'Gamma Bed',
        mdate: new Date(2020,9,22),
        price: 6377000,
        imgUrl: 'gamma-bed.jpg',
        length: 2.22,
        width: 1.93,
        height: 0.39,
        cate: 'bed'
    }
]

export default furnitures;