import React, { useContext } from 'react';
import PropTypes from 'prop-types';

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import CartContext from '../../contexts/CartContext';
import AlertBox from '../AlertBox/AlertBox';

const Shopitem = ({ item }) => {
  let img_src = require('../../assets/images/' + item.imgUrl);
  const { cart, setCart } = useContext(CartContext);

  const addToCart = () => {
    let items = [...cart];
    if (items.findIndex(x => x.id == item.id) > -1) return;
    items.push({
      id: item.id,
      fname: item.fname,
      price: item.price,
      imgUrl: item.imgUrl,
      quan: 1
    });
    setCart(items);
    sessionStorage.setItem('cart', JSON.stringify(items));
    setOpen(true);
  };

  // alert
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Card sx={{ maxWidth: 345 }}>
        <CardMedia
          component="img"
          alt="green iguana"
          height="140"
          src={img_src}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            {item.fname}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Manufacture date: {item.mdate.toLocaleDateString("en-GB")}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Price: {item.price.toLocaleString()}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Length: {item.length} m
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Width: {item.width} m
          </Typography>
          <Typography variant="body2" color="text.secondary">
            Height: {item.height} m
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" onClick={addToCart}>Add to card</Button>
        </CardActions>
      </Card>
      <AlertBox
        open={open}
        setOpen={setOpen}
        handleClose={handleClose}
        title='Item was added to cart'
        text={`Item named ${item.fname} has been added to cart! Check your cart now.`}
      />
    </>
  );
};

Shopitem.propTypes = {};

Shopitem.defaultProps = {};

export default Shopitem;
