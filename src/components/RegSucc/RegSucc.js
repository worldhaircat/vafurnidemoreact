import React from 'react';
import PropTypes from 'prop-types';
import styles from './RegSucc.module.css';

const RegSucc = () => (
  <div className={styles.RegSucc}>
    You have successfully register new account!
  </div>
);

RegSucc.propTypes = {};

RegSucc.defaultProps = {};

export default RegSucc;
