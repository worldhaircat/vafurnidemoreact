import { Box } from "@mui/material";

const FootEnd = () => (
  <div className='wrapper1'>
    <div className='wrapper2'>
      <Box sx={{ display: 'flex' }}>
        <Box sx={{ flex: 3, display: 'flex', alignItems: 'center', justifyContent: 'left', margin: '0 20px' }}>
          <p style={{ margin: 0 }}>&copy; VaFurni, 2022</p>
        </Box>
        <Box sx={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <p>Terms & Conditions</p>
        </Box>
        <Box sx={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <p>Cookies </p>
        </Box>
        <Box sx={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <p>Privacy Policy</p>
        </Box>
      </Box>
    </div>
  </div>
);

FootEnd.propTypes = {};

FootEnd.defaultProps = {};

export default FootEnd;