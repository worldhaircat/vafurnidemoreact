import { Box, Grid } from "@mui/material";

import HouseIcon from '@mui/icons-material/House';
import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import EmailIcon from '@mui/icons-material/Email';

const FootSec04 = () => (
  <Grid
    item
    sx={{ padding: 2 }}
    lg={3}
    md={6}
    xs={12}
  >
    <h4>CONTACT US</h4>
    <Box sx={{ display: 'flex' }}>
      <Box sx={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'left' }}>
        <HouseIcon sx={{ color: 'white' }} />
      </Box>
      <Box sx={{ flex: 5 }}>
        <p>123 ABC street</p>
      </Box>
    </Box>
    <Box sx={{ display: 'flex' }}>
      <Box sx={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'left' }}>
        <LocalPhoneIcon sx={{ color: 'white' }} />
      </Box>
      <Box sx={{ flex: 5 }}>
        <p>091 xxx 454545</p>
      </Box>
    </Box>
    <Box sx={{ display: 'flex' }}>
      <Box sx={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'left' }}>
        <EmailIcon sx={{ color: 'white' }} />
      </Box>
      <Box sx={{ flex: 5 }}>
        <p>vafurni-xxx@email.net</p>
      </Box>
    </Box>

  </Grid>
);

FootSec04.propTypes = {};

FootSec04.defaultProps = {};

export default FootSec04;