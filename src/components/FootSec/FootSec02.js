import { Avatar, Box, Grid } from "@mui/material";

const FootSec02 = () => (
  <Grid
    item
    sx={{ padding: 2 }}
    lg={3}
    md={6}
    xs={12}
  >
    <h4>FROM THE BLOG</h4>
    <Box sx={{ display: 'flex' }}>
      <Box sx={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        <Avatar
          alt="blog-ava1"
          src={require('../../assets/subImg/blog-furni.jpg')}
          sx={{ width: 56, height: 56 }}
        />
      </Box>
      <Box sx={{ flex: 2 }}>
        <h5>Fun chairs</h5>
        <p>12/03/2022</p>
      </Box>
    </Box>

    <hr style={{ border: '0.5px dashed #D3D3D3' }} />
    <Box sx={{ display: 'flex' }}>
      <Box sx={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        <Avatar
          alt="blog-ava1"
          src={require('../../assets/subImg/blog-2.jpg')}
          sx={{ width: 56, height: 56 }}
        />
      </Box>
      <Box sx={{ flex: 2 }}>
        <h5>Love Furni</h5>
        <p>29/09/2021</p>
      </Box>
    </Box>
  </Grid>
);

FootSec02.propTypes = {};

FootSec02.defaultProps = {};

export default FootSec02;