import { Grid } from "@mui/material";

const FootSec01 = () => (
    <Grid
        item
        sx={{ padding: 2 }}
        lg={3}
        md={6}
        xs={12}
    >
        <h4>ABOUT US</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tempor mauris ut gravida consequat. Suspendisse lacus velit, rhoncus sit amet aliquet in, pulvinar quis elit. Nulla ac quam sed orci mollis condimentum. Nam aliquam aliquet nisi vel fringilla.</p>
    </Grid>
);

FootSec01.propTypes = {};

FootSec01.defaultProps = {};

export default FootSec01;