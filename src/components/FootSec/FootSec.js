import React from 'react';
import PropTypes from 'prop-types';
import styles from './FootSec.module.css';
import {Grid} from '@mui/material';

import FootSec01 from './FootSec01';
import FootSec02 from './FootSec02';
import FootSec03 from './FootSec03';
import FootSec04 from './FootSec04';
import FootEnd from './FootEnd';

const FootSec = () => {

  return (
    <footer className={styles.FootSec}>
      <div className='wrapper1'>
        <div className='wrapper2'>
          <Grid container
            sx={{ width: '100%' }}
          >
            <FootSec01 />
            <FootSec02 />
            <FootSec03 />
            <FootSec04 />
          </Grid>
        </div>
      </div>

      <hr style={{ border: 'none', height: '0.5px', backgroundColor: 'white' }} />
      <FootEnd />
    </footer>
  );
};

FootSec.propTypes = {};

FootSec.defaultProps = {};

export default FootSec;
