import { Avatar, Box, Grid } from "@mui/material";

const FootSec03 = () => (
  <Grid
    item
    sx={{ padding: 2 }}
    lg={3}
    md={6}
    xs={12}
  >
    <h4>LATEST TWEETS</h4>
    <Box sx={{ display: 'flex' }}>
      <Box sx={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        <Avatar
          alt="blog-ava1"
          src={require('../../assets/subImg/small-icon.jpg')}
          sx={{ width: 45, height: 45 }}
        />
      </Box>
      <Box sx={{ flex: 2 }}>
        <h5>Enlolo</h5>
        <p>@enlolo</p>
      </Box>
    </Box>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tempor mauris ut gravida consequat. Suspendisse lacus velit, rhoncus sit amet aliquet in, pulvinar quis elit. Nulla ac quam sed orci mollis condimentum. Nam aliquam aliquet nisi vel fringilla.</p>
  </Grid>
);

FootSec03.propTypes = {};

FootSec03.defaultProps = {};

export default FootSec03;