import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import users from '../../fakeapi/users';
import { useNavigate } from 'react-router-dom';
import { Box, Button, Stack, TextField } from '@mui/material';
import { calculateNewValue } from '@testing-library/user-event/dist/utils';

const Register = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isErrUsername, setErrUsername] = useState(false);
  const [isErrPassword, setErrPassword] = useState(false);
  let navi = useNavigate();

  useEffect(() => window.scrollTo(0, 0), []);

  const handleChangeUsername = e => {
    let regex = /^[A-Za-z]\w*$/;
    let _name = e.target.value;
    setErrUsername(_name.trim() == '' || !(regex.test(_name)));
    setUsername(_name);
  }

  const handleChangePassword = e => {
    let regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,20}$/;
    let _pw = e.target.value;
    setErrPassword(_pw.trim() == '' || !(regex.test(_pw)));
    setPassword(_pw);
  }

  const isErrValidate = () => {
    let _name = username.trim() === '';
    setErrUsername(_name);
    let _pw = password.trim() === '';
    setErrPassword(_pw);
    return _name || _pw;
  }

  const handleRegister = e => {
    e.preventDefault();
    if(isErrValidate()) return;
    let maxId = users.length > 0 ? users[users.length - 1].id : 0;
    users.push({
      id: maxId + 1,
      username: username,
      password: password
    });
    navi('/regsucc')
  };

  return (
    <div style={{
      margin: 0,
      padding: 0,
      overflowX: 'hidden',
      display: 'flex',
      justifyContent: 'center',
      width: '100vw'
    }}>
      <Box>
        <h2>Register new account</h2>
        <form style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
          <Stack
            spacing={2}
          >
            <TextField
              id='login-username'
              label='Username'
              variant="outlined"
              error={!!isErrUsername}
              helperText='Username must not be emtpy and must be in right format'
              value={username}
              onChange={handleChangeUsername}
            />
            <TextField
              id='login-password'
              label='Password'
              variant="outlined"
              type='password'
              error={!!isErrPassword}
              helperText='Password must have minimum eight and maximum 20 characters, at least one uppercase letter, one lowercase letter, one number and one special character'
              value={password}
              onChange={handleChangePassword}
            />
          </Stack>
          <Button
            type='submit'
            variant='contained'
            color='primary'
            sx={{ m: 2, color: 'white' }}
            onClick={e => handleRegister(e)}
          >Confirm register</Button>
        </form>
      </Box>
    </div>

  );
};

Register.propTypes = {};

Register.defaultProps = {};

export default Register;
