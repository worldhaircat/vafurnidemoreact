import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Shopitem from '../Shopitem/Shopitem';
import furnitures from '../../fakeapi/furnitures';
import { FormControl, Grid, InputLabel, MenuItem, Select, Switch, Typography } from "@mui/material";
import CateFilter from './CateFilter';
import NameSearch from './NameSearch';
import { Box } from '@mui/system';

const Shop = () => {
  let [products, setProducts] = useState([]);
  let [checked, setChecked] = useState(false);
  let [cate, setCate] = useState('');
  let [flag, setFlag] = useState(false);
  useEffect(() => {
    window.scrollTo(0, 0);
    setProducts(furnitures);
  }, []);

  const establishProducts = (_checked, _cate) => {
    let list = [];
    // filter by cate
    if (_cate == 'all' || _cate == '') {
      list = furnitures;
    } else {
      list = furnitures.filter(x => x.cate == _cate);
    }
    //filter by year
    if (_checked) {
      list = list.filter(f => f.mdate.getFullYear() == new Date().getFullYear());
    }
    setFlag(!flag);
    setProducts(list);
  };

  // filterYear
  const handleFilterYear = e => {
    setChecked(e.target.checked);
    establishProducts(e.target.checked, cate);
  };

  // catefilter
  const handleFilterChange = e => {
    setCate(e.target.value);
    establishProducts(checked, e.target.value);
  };

  // namesearch
  const onHandleNameSearch = (e, values) => {
    if (values == null) {
      establishProducts(checked, cate);
      return;
    }
    setProducts([values]);
  }

  // styles for shop head
  const headStyles = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    padding: '20px'
  };

  return (
    <>
      <div className='wrapper1'>
        <div className='wrapper2'>
          <Box sx={{ display: 'flex', width: '100%' }}>
            <Box sx={headStyles}>
              <Typography variant="caption" display="block" gutterBottom>Filter furnitures manufactured in this year</Typography>
              <Switch checked={checked} onChange={handleFilterYear} />
            </Box>
            <Box sx={headStyles}>
              <Typography variant="caption" display="block" gutterBottom>Filter by category</Typography>
              <CateFilter cate={cate} handleFilterChange={handleFilterChange} />
            </Box>
            <Box sx={headStyles}>
              <Typography variant="caption" display="block" gutterBottom>Search by name</Typography>
              <NameSearch products={products} onHandleNameSearch={onHandleNameSearch} flag={flag} />
            </Box>
          </Box>
          <Grid container spacing={2} sx={{ width: '100%' }}>
            {products.map(product =>
              <Grid item
                key={product.id}
                lg={3}
                md={4}
                sm={6}
              >
                <Shopitem item={product} />
              </Grid>
            )
            }
          </Grid>
        </div>
      </div>
    </>
  );
};

Shop.propTypes = {};

Shop.defaultProps = {};

export default Shop;
