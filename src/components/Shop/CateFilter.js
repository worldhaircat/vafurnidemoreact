import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";

const CateFilter = ({cate, handleFilterChange}) => (
    <FormControl variant="standard" sx={{ m: 1, minWidth: 120, width: '20vw' }}>
        <InputLabel id="simple-select-label">Filter by category</InputLabel>
        <Select
            labelId="simple-select-label"
            id="simple-select"
            value={cate}
            label="Category"
            onChange={handleFilterChange}
        >
            <MenuItem value="desk">Desk</MenuItem>
            <MenuItem value="chair">Chair</MenuItem>
            <MenuItem value="bed">Bed</MenuItem>
            <MenuItem value="all">All</MenuItem>
        </Select>
    </FormControl>
);

CateFilter.propTypes = {};

CateFilter.defaultProps = {};

export default CateFilter;