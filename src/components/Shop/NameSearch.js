import { Autocomplete, TextField } from "@mui/material";

const NameSearch = ({products, onHandleNameSearch, flag}) => (
  <Autocomplete
    disablePortal
    id="combo-box"
    options={products}
    getOptionLabel={p => p.fname}
    sx={{ width: 300 }}
    renderInput={params => <TextField {...params} label="Furniture" />}
    onChange={onHandleNameSearch}
    key={flag}
  />
)

NameSearch.propTypes = {};

NameSearch.defaultProps = {};

export default NameSearch;