import React, { useContext, useEffect } from 'react';
import PropTypes from 'prop-types';

import CartContext from '../../contexts/CartContext';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import CartItem from '../CartItem/CartItem';
import { Button, Typography } from '@mui/material';
import AlertBox from '../AlertBox/AlertBox';
import { Box } from '@mui/system';

const Cart = () => {
  const { cart, setCart } = useContext(CartContext);
  const total = cart.length != 0 ? cart.map(x => x.quan * x.price).reduce((a, b) => a + b) : 0;

  useEffect(() => window.scrollTo(0, 0), []);
  const handleAdd = itemId => {
    let rows = [...cart];
    let i = rows.findIndex(x => x.id == itemId);
    rows[i].quan++;
    saveCart(rows);
  };
  const handleSubtract = itemId => {
    let rows = [...cart];
    let i = rows.findIndex(x => x.id == itemId);
    if (rows[i].quan > 1)
      rows[i].quan--;
      saveCart(rows);
  };
  const handleDelete = itemId => {
    let rows = [...cart];
    let i = rows.findIndex(x => x.id == itemId);
    rows.splice(i, 1);
    saveCart(rows);
  };
  const handleCheckOut = () => {
    saveCart([]);
    setOpen(true);
  };
  const saveCart = rows => {
    setCart(rows);
    sessionStorage.setItem('cart', JSON.stringify(rows));
  }

  // alert
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  // table head styles
  const tableHeadStyles = {
    fontWeight: 'bold'
  };

  return (
    <div className='wrapper1'>
      <div className='wrapper2'>
        <Box sx={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
          <Typography variant="h3" gutterBottom>YOUR CHECKING OUT CART</Typography>
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell sx={tableHeadStyles} align="center">Image</TableCell>
                  <TableCell sx={tableHeadStyles} align="left">Name</TableCell>
                  <TableCell sx={tableHeadStyles} align="right">Price</TableCell>
                  <TableCell sx={tableHeadStyles} align="center">Adjust Quantity</TableCell>
                  <TableCell sx={tableHeadStyles} align="right">Quantity</TableCell>
                  <TableCell sx={tableHeadStyles} align="right">Subtotal</TableCell>
                  <TableCell sx={tableHeadStyles} align="center">Remove Item</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {cart.map((row) => (
                  <CartItem
                    key={row.id}
                    row={row}
                    onAdd={handleAdd}
                    onSubtract={handleSubtract}
                    onDelete={handleDelete}
                  ></CartItem>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <h5 style={{ color: 'black' }}>Total: {total}</h5>
          <Button onClick={handleCheckOut}>Check Out</Button>
          <AlertBox
            open={open}
            setOpen={setOpen}
            handleClose={handleClose}
            title='Check out done'
            text='You have checked out!'
          />
        </Box>
      </div>
    </div>
  );
};

Cart.propTypes = {};

Cart.defaultProps = {};

export default Cart;
