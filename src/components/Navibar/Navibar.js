import React from 'react';
import PropTypes from 'prop-types';

import { Routes, Route, Navigate } from "react-router-dom";
import Home from '../Home/Home';
import Shop from '../Shop/Shop';

import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Link } from 'react-router-dom';
import Cart from '../Cart/Cart';
import Login from '../Login/Login';
import Register from '../Register/Register';
import RegSucc from '../RegSucc/RegSucc';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import { Badge } from '@mui/material';
import CartContext from '../../contexts/CartContext';

const drawerWidth = 240;
const navItems = [
  {
    title: 'Home',
    link: '/'
  },
  {
    title: 'Shop',
    link: '/shop'
  },
];

const Navibar = props => {
  const { window } = props;

  const cartContext = React.useContext(CartContext);
    const numberofCartItems = cartContext.cart.reduce((total, item) => {
      return total + item.quan;
    }, 0);

  const [mobileOpen, setMobileOpen] = React.useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  let { isLoggedIn, setIsLoggedIn, logOut } = props.loginLogout;

  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
      <Typography variant="h6" sx={{ my: 2 }}>
        VaFurni
      </Typography>

      <Divider />
      <List>
        {navItems.map((item) => (
          <ListItem key={item.title} disablePadding>
            <ListItemButton component={Link} to={item.link} sx={{ textAlign: 'center' }}>
              <ListItemText primary={item.title} />
            </ListItemButton>
          </ListItem>
        ))}
        {isLoggedIn ? (
          <>
            <ListItem disablePadding>
              <ListItemButton component={Link} to='/cart' sx={{ textAlign: 'center' }}>
                <ListItemText>
                  <Badge badgeContent={numberofCartItems} color="primary">
                    Your cart
                  </Badge>
                </ListItemText>
              </ListItemButton>
            </ListItem>
            <ListItem disablePadding>
              <ListItemButton href='' onClick={e => logOut(e)} sx={{ textAlign: 'center' }}>
                <ListItemText primary='Log out' />
              </ListItemButton>
            </ListItem>
          </>
        ) : (
          <>
            <ListItem disablePadding>
              <ListItemButton component={Link} to='/register' sx={{ textAlign: 'center' }}>
                <ListItemText primary='Register' />
              </ListItemButton>
            </ListItem>
            <ListItem disablePadding>
              <ListItemButton component={Link} to='/login' sx={{ textAlign: 'center' }}>
                <ListItemText primary='Log in' />
              </ListItemButton>
            </ListItem>
          </>
        )}
      </List>

    </Box>
  );

  const container = window !== undefined ? () => window().document.body : undefined;

  return (
    <Box sx={{ display: 'flex' }}>
      <AppBar component="nav" color='secondary'>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            sx={{ mr: 2, display: { sm: 'none' } }}
          >
            <MenuIcon />
          </IconButton>
          <Typography
            variant="h6"
            component="div"
            sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}
          >
            VaFurni
          </Typography>
          <Box sx={{ display: { xs: 'none', sm: 'block' } }}>
            {navItems.map((item) => (
              <Button key={item.title} component={Link} to={item.link} sx={{ color: '#fff' }}>
                {item.title}
              </Button>
            ))}
            {isLoggedIn ? (
              <>
                <IconButton component={Link} to='/cart'>
                  <Badge badgeContent={numberofCartItems} color="primary">
                    <ShoppingCartIcon sx={{ color: 'white' }} />
                  </Badge>
                </IconButton>
                <Button href='' onClick={e => logOut(e)} sx={{ color: '#fff' }}>Log out</Button>
              </>
            ) : (
              <>
                <Button component={Link} to='/register' sx={{ color: '#fff' }}>
                  Register
                </Button>
                <Button component={Link} to='/login' sx={{ color: '#fff' }}>
                  Log in
                </Button>
              </>
            )}
          </Box>
        </Toolbar>
      </AppBar>
      <Box component="nav">
        <Drawer
          container={container}
          variant="temporary"
          open={mobileOpen}
          onClose={handleDrawerToggle}
          ModalProps={{
            keepMounted: true, // Better open performance on mobile.
          }}
          sx={{
            display: { xs: 'block', sm: 'none' },
            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
          }}

        >
          {drawer}
        </Drawer>
      </Box>
      <Box component="main" sx={{ pt: 0, pb: 3, minHeight: '100vh' }}>
        <Toolbar />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/shop" element={isLoggedIn ? (<Shop></Shop>) : (<Navigate to="/login" />)} />
          <Route path="/cart" element={isLoggedIn ? (<Cart></Cart>) : (<Navigate to="/login" />)} />
          <Route path="/register" element={isLoggedIn ? (<Navigate to="/" />) : (
            <Register></Register>
          )} />
          <Route path="/login" element={isLoggedIn ? (<Navigate to="/" />) : (
            <Login tog={setIsLoggedIn}></Login>
          )} />
          <Route path='/regsucc' element={<RegSucc />} />
        </Routes>
      </Box>
    </Box>
  );
};

Navibar.propTypes = {};

Navibar.defaultProps = {};

export default Navibar;
