import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import styles from './Login.module.css';
import { Box, Button, TextField } from '@mui/material';
import { Stack } from '@mui/system';
import users from '../../fakeapi/users';
import { useNavigate } from 'react-router-dom';
import AlertBox from '../AlertBox/AlertBox';

const Login = ({ tog }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isErrUsername, setErrUsername] = useState(false);
  const [isErrPassword, setErrPassword] = useState(false);
  let navi = useNavigate();

  useEffect(() => window.scrollTo(0, 0), []);

  const handleChangeUsername = e => {
    let _name = e.target.value;
    setErrUsername(_name.trim() == '');
    setUsername(_name);
  }

  const handleChangePassword = e => {
    let _pw = e.target.value;
    setErrPassword(_pw.trim() == '');
    setPassword(_pw);
  }

  const isErrValidate = () => {
    let _name = username.trim() === '';
    setErrUsername(_name);
    let _pw = password.trim() === '';
    setErrPassword(_pw);
    return _name || _pw;
  }

  const handleLogin = e => {
    e.preventDefault();
    if(isErrValidate()) return;
    let flag = false;
    for (let x of users) {
      if (x.username == username && x.password == password) flag = true;
    }
    if (flag) {
      sessionStorage.setItem('username', username);
      tog(true);
      navi('/shop');
    } else {
      setOpen(true);
    }
  };

  // alert
  const [open, setOpen] = React.useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div className={styles.Login}>
      <Box>
        <h2>Log in to your account</h2>
        <form>
          <Stack
            spacing={2}
          >
            <TextField
              id='login-username'
              label='Username'
              variant="outlined"
              error={!!isErrUsername}
              helperText='Username must not be emtpy'
              value={username}
              onChange={handleChangeUsername}
            />
            <TextField
              id='login-password'
              label='Password'
              variant="outlined"
              type='password'
              error={!!isErrPassword}
              helperText='Password must not be emtpy'
              value={password}
              onChange={handleChangePassword}
            />
          </Stack>
          <Button
            type='submit'
            variant='contained'
            color='primary'
            sx={{ m: 2, color: 'white' }}
            onClick={handleLogin}
          >Confirm log in</Button>
        </form>
      </Box>

      <AlertBox
        open={open}
        setOpen={setOpen}
        handleClose={handleClose}
        title='Log in failed'
        text='Wrong username or password or both!'
      />
    </div>
  );
};

Login.propTypes = {};

Login.defaultProps = {};

export default Login;
