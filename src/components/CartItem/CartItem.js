import React from 'react';
import PropTypes from 'prop-types';

import TableRow from '@mui/material/TableRow';
import TableCell from '@mui/material/TableCell';
import { Button, IconButton, Tooltip } from '@mui/material';
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import RemoveCircleOutlineIcon from '@mui/icons-material/RemoveCircleOutline';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';

const CartItem = ({ row, onAdd, onSubtract, onDelete }) => {
  let img_src = require('../../assets/images/' + row.imgUrl);
  return (
    <>
      <TableRow
        key={row.name}
        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
      >
        <TableCell align="center">
          <img 
            style={{ width: '100px', height: '100px', objectFit: 'cover'}}
            alt={row.name}
            src={img_src}
          />
        </TableCell>
        <TableCell>{row.fname}</TableCell>
        <TableCell align="right">{row.price}</TableCell>
        <TableCell align="center">
          <Tooltip title="Add">
            <IconButton onClick={() => onAdd(row.id)}>
              <AddCircleOutlineIcon color='primary' />
            </IconButton>
          </Tooltip>
          <Tooltip title="Subtract">
            <IconButton onClick={() => onSubtract(row.id)}>
              <RemoveCircleOutlineIcon color='primary' />
            </IconButton>
          </Tooltip>
        </TableCell>
        <TableCell align="right">{row.quan}</TableCell>
        <TableCell align="right">{row.quan*row.price}</TableCell>
        <TableCell align="center">
          <Tooltip title="Remove">
            <IconButton onClick={() => onDelete(row.id)}>
              <HighlightOffIcon color='primary' />
            </IconButton>
          </Tooltip>
        </TableCell>
      </TableRow>
    </>
  );
};

CartItem.propTypes = {};

CartItem.defaultProps = {};

export default CartItem;
