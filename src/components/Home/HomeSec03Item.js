import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styles from './Home.module.css';

import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import cusTheme from '../../helper/cusTheme';

const HomeSec03Item = ({ item }) => {
  let img_src = require('../../assets/images/' + item.imgUrl);
  let smallBoxStyles = {
    flex: '1',
    width: '100%',
    padding: '10px'
  };
  // item.fname item.mdate.toLocaleDateString("en-GB") item.price.toLocaleString() item.length item.width item.height
  return (
    <Card sx={{ width: '100%', margin: '0 20px' }}>
      <Box sx={{ position: 'relative', borderBottom: 'solid 5px ' + cusTheme.palette.primary.main }}>
        <CardMedia
          component="img"
          sx={{ width: '100%', height: '200px', objectFit: 'cover' }}
          alt={item.fname}
          src={img_src}
        />
        <Box sx={{
          position: 'absolute',
          bottom: 0,
          right: 0,
          width: '150px',
          height: '30px',
          backgroundColor: cusTheme.palette.primary.main,
          color: 'white',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center'
        }}>
          {item.price.toLocaleString()} vnd
        </Box>
      </Box>
      <Box sx={{ padding: '20px' }}>
        <Typography variant="h5" sx={{ color: 'black' }} gutterBottom>
          {item.fname}
        </Typography>
        <Typography variant="body1" sx={{ color: 'black' }} gutterBottom>
          Manufactured Date: {item.mdate.toLocaleDateString("en-GB")}
        </Typography>
      </Box>
      <Box sx={{ display: 'flex', borderTop: 'solid 1px lightgrey' }}>
        <Box sx={{ ...smallBoxStyles, borderRight: 'solid 1px lightgrey' }}>
          <Typography variant="caption" sx={{ color: 'black' }} gutterBottom>
            Length
          </Typography>
          <Typography variant="body1" sx={{ color: 'black', textAlign: 'center' }} gutterBottom>
            {item.length}
          </Typography>
        </Box>
        <Box sx={{ ...smallBoxStyles, borderRight: 'solid 1px lightgrey' }}>
          <Typography variant="caption" sx={{ color: 'black' }} gutterBottom>
            Width
          </Typography>
          <Typography variant="body1" sx={{ color: 'black', textAlign: 'center' }} gutterBottom>
            {item.width}
          </Typography>
        </Box>
        <Box sx={{ ...smallBoxStyles}}>
          <Typography variant="caption" sx={{ color: 'black' }} gutterBottom>
            Height
          </Typography>
          <Typography variant="body1" sx={{ color: 'black', textAlign: 'center' }} gutterBottom>
            {item.height}
          </Typography>
        </Box>
      </Box>
    </Card>
  );
};

HomeSec03Item.propTypes = {};

HomeSec03Item.defaultProps = {};

export default HomeSec03Item;