import React from 'react';
import PropTypes from 'prop-types';
import styles from './Home.module.css';

import Box from '@mui/material/Box';
import HomeSec03Item from './HomeSec03Item';

import furnitures from '../../fakeapi/furnitures';
import darkImg from '../../assets/subImg/dark-furni.jpg';
import Slideshow from './Slideshow';
import { Typography } from '@mui/material';

const HomeSec03 = () => {
  let items = furnitures.slice(0, 5);
  return (
    <Box sx={{
      position: 'relative'
    }}>
      <img src={darkImg} style={{
        width: '100vw',
        height: '735px',
        objectFit: 'cover'
      }} />
      <Box sx={{
        width: '100vw',
        padding: '10px',
        position: 'absolute',
        top: 0,
        left: 0
      }}>
        <Typography variant="h5" gutterBottom sx={{textAlign: 'center', paddingTop: '40px', fontFamily: 'Snell Roundhand, cursive'}}>Take a look at our</Typography>
        <Typography variant="h3" gutterBottom sx={{textAlign: 'center', color: 'white'}}>MOST POPULAR PRODUCTS</Typography>
        <div className='wrapper1'>
          <div className='wrapper2'>
            <Slideshow>
              {
                items.map(item => <div key={item.id} className={styles.carouselInside}>
                  <HomeSec03Item item={item} />
                </div>
                )
              }
            </Slideshow>
          </div>
        </div>
      </Box>
    </Box>

  );
};

HomeSec03.propTypes = {};

HomeSec03.defaultProps = {};

export default HomeSec03;