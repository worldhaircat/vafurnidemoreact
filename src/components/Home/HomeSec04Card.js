import React from 'react';
import { Box } from '@mui/system';
import { Avatar, Rating} from '@mui/material';

const HomeSec04Card = ({ cusname, avacolour, title, content }) => {
  return (
    <Box
      sx={{
        display: 'flex',
        justifyContent: 'center'
      }}
    >
      <Box sx={{ flex: 1, display: 'flex', alignItems: 'center', justifyContent: 'center', flexDirection: 'column', margin: '10px' }}>
        <Avatar
          alt="review-ava"
          sx={{ width: 56, height: 56, bgcolor: avacolour }}
        />
        <p>{cusname}</p>
      </Box>
      <Box sx={{ flex: 3 }}>
        <h5 style={{color: 'black'}}>{title}</h5>
        <p>{content}</p>
        <Rating value={5} readOnly />
      </Box>
    </Box>
  );
};

HomeSec04Card.propTypes = {};

HomeSec04Card.defaultProps = {};

export default HomeSec04Card;