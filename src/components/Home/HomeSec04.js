import React from 'react';
import styles from './Home.module.css';
import { Grid, Typography } from '@mui/material';
import HomeSec04Card from './HomeSec04Card';

const HomeSec04 = () => {
  let cards = [
    {
      id: 1,
      cusname: 'Alice',
      avacolour: 'crimson',
      title: 'Good experience',
      content: 'Suspendisse lacus velit, rhoncus sit amet aliquet in, pulvinar quis elit. Nulla ac quam sed orci mollis condimentum.'
    },
    {
      id: 2,
      cusname: 'Ben',
      avacolour: 'darkblue',
      title: 'Nice service',
      content: 'Suspendisse lacus velit, rhoncus sit amet aliquet in, pulvinar quis elit. Nulla ac quam sed orci mollis condimentum.'
    },
    {
      id: 3,
      cusname: 'Minh',
      avacolour: 'purple',
      title: 'Beautiful products',
      content: 'Suspendisse lacus velit, rhoncus sit amet aliquet in, pulvinar quis elit. Nulla ac quam sed orci mollis condimentum.'
    },
    {
      id: 4,
      cusname: 'John',
      avacolour: 'green',
      title: 'Will come again',
      content: 'Suspendisse lacus velit, rhoncus sit amet aliquet in, pulvinar quis elit. Nulla ac quam sed orci mollis condimentum.'
    },
  ];

  return (
    <div className='wrapper1'>
      <div className='wrapper2'>
        <Typography
          variant="h3"
          gutterBottom
          sx={{ pb: 4, pt: 2, marginRight: '50px', textAlign: 'right' }}
        >
          Customer Reviews
        </Typography>
        <Grid container spacing={2} sx={{ width: '100%', mb: '40px' }}>
          {cards.map(card =>
            <Grid
              item
              key={card.id}
              lg={6}
              md={6}
              sm={6}
            >
              <HomeSec04Card 
                cusname={card.cusname}
                avacolour={card.avacolour}
                title={card.title}
                content={card.content}
              />
            </Grid>
          )}
        </Grid>
      </div>
    </div>
  );
};

HomeSec04.propTypes = {};

HomeSec04.defaultProps = {};

export default HomeSec04;