import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styles from './Home.module.css';
import HomeSec01 from './HomeSec01';
import HomeSec02 from './HomeSec02';
import HomeSec03 from './HomeSec03';
import HomeSec04 from './HomeSec04';

const Home = () => {
  useEffect(() => window.scrollTo(0, 0), []);
  return (
    <div className={styles.Home}>
      <HomeSec01 />
      <HomeSec02 />
      <HomeSec03 />
      <HomeSec04 />
    </div>
  );
};

Home.propTypes = {};

Home.defaultProps = {};

export default Home;
