import { useEffect, useState } from "react";

const buttonCommonStyle = {
  position: 'absolute',
  top: '50%',
  transform: 'translate(0%,-50%)',
  msTransform: 'translate(-0%,-50%)',
  fontSize: '50px',
  backgroundColor: 'rgba(0,0,0,0)',
  border: 'solid 0px',
  color: 'white'
};

const Slideshowcore = ({ shownSlides, shownLength, movingTime, flip, showSlides, startcheck, setStartcheck, moveIndex, bigWidth }) => {
  useEffect(() => {
    if (startcheck) {
      setStartcheck(false);
    } else {
      setTimeout(() => {
        showSlides(shownLength, moveIndex);
      }, 1);
    }
    console.log('Slideshowcore rendered');
  }, [flip]);
  return (
    <div id="slideshow-container" style={{
      display: 'flex',
      WebkitTransition: movingTime,
      transition: movingTime,
      marginLeft: 0,
      position: 'absolute'
    }}>
      {shownSlides.map((x, i) =>
        <div
          key={i}
          style={{
            width: bigWidth / shownLength,
            height: '420px',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
          }}
        >{x}</div>
      )}
    </div>
  )
}

const Slideshow = ({ children }) => {
  let [slideIndex, setSlideIndex] = useState(0);
  let [shownSlides, setShownSlides] = useState([]);
  let [shownLength, setShownLength] = useState(0);
  let [movingTime, setMovingTime] = useState('0s');
  let [flip, setFlip] = useState(-1);
  let [startcheck, setStartcheck] = useState(true);
  let [moveIndex, setMoveIndex] = useState(0);
  let [bigWidth, setBigWidth] = useState('100px');
  let slides = children;

  let switchLen = () => {
    if (window.innerWidth > 900 && (shownLength != 3 || shownLength == 0)) {
      showSlides(3);
      setShownLength(3);
    } else if (window.innerWidth <= 900 && window.innerWidth > 600 && (shownLength != 2 || shownLength == 0)) {
      showSlides(2);
      setShownLength(2);
    } else if (window.innerWidth <= 600 && (shownLength != 1 || shownLength == 0)) {
      showSlides(1);
      setShownLength(1);
    }
  };
  let handleResize = () => {
    switchLen();
    if (moveIndex === 1) plusSlides(0);
  };
  let handleBigWidthChange = () => {
    let bigContainerWidth = document.getElementById('big-slideshow-container').offsetWidth;
    setBigWidth(bigContainerWidth);
  };

  useEffect(() => {
    switchLen();
    setBigWidth(document.getElementById('big-slideshow-container').offsetWidth);
    window.addEventListener('resize', handleResize);
    window.addEventListener('resize', handleBigWidthChange);
    return () => {
      window.removeEventListener('resize', handleResize);
      window.removeEventListener('resize', handleBigWidthChange);
    }
  }, [slideIndex, bigWidth]);

  let plusSlides = n => {
    setFlip(0 - flip);
    showSlides(shownLength);
    let container = document.getElementById('slideshow-container');
    container.style.marginLeft = n === -1 ? '-' + bigWidth / shownLength + 'px' : 0;
    setMovingTime('0s');
    setMoveIndex(n);
  }

  let showSlides = (itemLen, move = 0) => {
    let x = move === -1 ? slideIndex - 1 : slideIndex;
    let shown = [];
    for (let j = 0 - Math.abs(move); j < itemLen; j++) {
      if (x > slides.length - 1) {
        x = 0;
      } else if (x < 0) {
        x = slides.length - 1;
      }
      if (
        j === (move === -1 ? -1 : 0)
      ) setSlideIndex(x);
      shown.push(slides[x]);
      x++;
    }
    if (move) {
      let container = document.getElementById('slideshow-container');
      container.style.marginLeft = move === -1 ? 0 : '-' + bigWidth / shownLength + 'px';
      setMovingTime('1s');
    }
    setShownSlides(shown);
  }

  return (
    <div style={{ position: 'relative', padding: '0 50px'}}>
      <div id='big-slideshow-container' style={{ width: '100%', height: '420px', position: 'relative', overflow: 'hidden' }}>
        <Slideshowcore
          shownSlides={shownSlides}
          movingTime={movingTime}
          shownLength={shownLength}
          flip={flip}
          setFlip={setFlip}
          showSlides={showSlides}
          startcheck={startcheck}
          setStartcheck={setStartcheck}
          moveIndex={moveIndex}
          bigWidth={bigWidth}
        />
      </div>
      <button
        className="prev"
        onClick={() => plusSlides(-1)}
        style={{
          left: '0',
          ...buttonCommonStyle
        }}
      >&#10094;</button>
      <button
        className="next"
        onClick={() => plusSlides(1)}
        style={{
          right: '0',
          ...buttonCommonStyle
        }}
      >&#10095;</button>
    </div>
  );
};

Slideshow.propTypes = {};

Slideshow.defaultProps = {};

export default Slideshow;