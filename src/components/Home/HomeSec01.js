import React from 'react';
import PropTypes from 'prop-types';
import styles from './Home.module.css';
import { Box, height } from '@mui/system';
import bgImg from '../../assets/subImg/furniture-home1.jpg';
import { Button, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

const HomeSec01 = () => (
    <Box sx={{
        position: 'relative'
    }}>
        <img src={bgImg} style={{
            width: '100vw',
            height: '700px',
            objectFit: 'cover'
        }} />
        <Box sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            textAlign: 'center'
        }}>
            <Typography 
                variant="h2" 
                gutterBottom 
                sx={{color: 'white', textShadow: '0 0 15px black'}}
            >
                Find your dream furniture
            </Typography>
            <Typography 
                variant="h1" 
                gutterBottom 
                sx={{color: 'white', textShadow: '0 0 15px black'}}
            >
                With our products
            </Typography>
            <Button
            variant='contained'
            color='primary'
            sx={{ m: 2, color: 'white' }}
            size="large"
            component={Link} to='/shop'
          >Go to shop</Button>
        </Box>
    </Box>
);

HomeSec01.propTypes = {};

HomeSec01.defaultProps = {};

export default HomeSec01;