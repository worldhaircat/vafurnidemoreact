import React from 'react';
import PropTypes from 'prop-types';
import styles from './Home.module.css';
import { Box } from '@mui/system';
import { Avatar, Typography } from '@mui/material';

// <Avatar sx={{ bgcolor: 'blue' }}>{lIcon.pic}</Avatar>

const HomeSec02Card = ({ icon, title, info }) => (
  <Box
    sx={{
      display: 'flex',
      justifyContent: 'center'
    }}
  >
    <Box
      sx={{ flex: 1 }}
    >
      <Avatar sx={{ 
        bgcolor: '#47a0ff',
        width: 60,
        height: 60
      }}>{icon}</Avatar>
    </Box>
    <Box
      sx={{ flex: 4, pl: 2 }}
    >
      <Typography variant="h5" gutterBottom sx={{color: 'black'}}>
        {title}
      </Typography>
      <Typography variant="subtitle1" gutterBottom>
        {info}
      </Typography>
    </Box>
  </Box>
);

HomeSec02Card.propTypes = {};

HomeSec02Card.defaultProps = {};

export default HomeSec02Card;