import React from 'react';
import PropTypes from 'prop-types';
import styles from './Home.module.css';
import { Grid, Typography } from '@mui/material';

import Avatar from '@mui/material/Avatar';
import LanguageIcon from '@mui/icons-material/Language';
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';
import CameraAltIcon from '@mui/icons-material/CameraAlt';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import HeadsetMicIcon from '@mui/icons-material/HeadsetMic';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import HomeSec02Card from './HomeSec02Card';

const HomeSec02 = () => {
  let cards = [
    {
      id: 1,
      icon: (<LanguageIcon />),
      title: 'Diversified',
      info: 'Suspendisse lacus velit, rhoncus sit amet aliquet in, pulvinar quis elit. Nulla ac quam sed orci mollis condimentum.'
    },
    {
      id: 2,
      icon: (<AttachMoneyIcon />),
      title: 'Not expensive',
      info: 'Suspendisse lacus velit, rhoncus sit amet aliquet in, pulvinar quis elit. Nulla ac quam sed orci mollis condimentum.'
    },
    {
      id: 3,
      icon: (<CameraAltIcon />),
      title: 'Beautiful',
      info: 'Suspendisse lacus velit, rhoncus sit amet aliquet in, pulvinar quis elit. Nulla ac quam sed orci mollis condimentum.'
    },
    {
      id: 4,
      icon: (<CalendarMonthIcon />),
      title: 'Fast deliver',
      info: 'Suspendisse lacus velit, rhoncus sit amet aliquet in, pulvinar quis elit. Nulla ac quam sed orci mollis condimentum.'
    },
    {
      id: 5,
      icon: (<HeadsetMicIcon />),
      title: 'Customer Support',
      info: 'Suspendisse lacus velit, rhoncus sit amet aliquet in, pulvinar quis elit. Nulla ac quam sed orci mollis condimentum.'
    },
    {
      id: 6,
      icon: (<FavoriteBorderIcon />),
      title: 'Passionate',
      info: 'Suspendisse lacus velit, rhoncus sit amet aliquet in, pulvinar quis elit. Nulla ac quam sed orci mollis condimentum.'
    }
  ];
  let lIcon = { pic: (<LanguageIcon />) };
  return (
    <div className='wrapper1'>
      <div className='wrapper2'>
        <Typography
          variant="h3"
          gutterBottom
          sx={{ pb: 4, pt: 2 }}
        >
          Why choose us?
        </Typography>
        <Grid container spacing={2} sx={{ width: '100%', mb: '40px' }}>
          {cards.map(card =>
            <Grid item
              key={card.id}
              lg={3}
              md={4}
              sm={6}
            >
              <HomeSec02Card
                icon={card.icon}
                title={card.title}
                info={card.info}
              />
            </Grid>
          )
          }
        </Grid>
      </div>
    </div>
  );
};

HomeSec02.propTypes = {};

HomeSec02.defaultProps = {};

export default HomeSec02;