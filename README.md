# Some basic information about this project

Project name: VafurniShop

Purpose of this project: To demonstrate some basic coding skills regarding to creating a React web project

Scope: This project is purely created with React framework and does not interact with a web server

Theme: This project simulates a furniture shop from end-customers' perspective

This project contains:
* a home page
* registering and logging in
* a shopping page showing a list of products
* a shopping cart for checking out products

Technologies that were used:
* Javascript
* React version 18.2.0
* React Dom version 18.2.0
* Mui (Material UI for React) version 11.10.0

# How to run this project

## Step 1: Install project library
Run the cmd: 'npm install' at vafurnishop folder

## Step 2: Start running project
Run the cmd: 'npm start' at vafurnishop folder

## Notice: How to change localhost port
Search for 'set PORT=' in package.json file
Change the port number